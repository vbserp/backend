<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 128)->nullable();
            $table->string('street', 128)->nullable();
            $table->string('number', 24)->nullable();
            $table->string('room', 24)->nullable();
            $table->string('fax', 24)->nullable();
            $table->string('city', 24)->nullable();
            $table->string('state', 24)->nullable();
            $table->string('postcode', 24)->nullable();
            $table->string('landline_phone', 24)->nullable();
            $table->string('cell_phone', 24)->nullable();
            $table->string('email', 128)->unique()->nullable();
            $table->text('observation')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
