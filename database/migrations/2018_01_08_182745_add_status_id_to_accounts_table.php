<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusIdToAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $tbl) {
            // Without `nullable()' the newly created column would have
            // no status_id and therefore there would be an error
            // attempting to add such column. It either needs a default
            // value or be nullable. Not sure what to really do. This
            // works for now.
            $tbl->integer('status_id')->unsigned()->nullable();
            $tbl->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $tbl) {
            $tbl->dropForeign('accounts_status_id_foreign');
            $tbl->dropColumn('status_id');
        });
    }
}
