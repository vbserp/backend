<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPersonIdToAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $tbl) {
            $tbl->integer('person_id')->unsigned()->nullable();
            $tbl->foreign('person_id')->references('id')->on('people');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $tbl) {
            //$tbl->dropForeign('accounts_person_id_foreign');
            $tbl->dropForeign(['person_id']);
            $tbl->dropColumn('person_id');
        });
    }
}
