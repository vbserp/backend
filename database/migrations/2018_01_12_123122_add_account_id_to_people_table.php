<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountIdToPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people', function (Blueprint $tbl) {
            $tbl->integer('account_id')->unsigned()->nullable();
            $tbl->foreign('account_id')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people', function (Blueprint $tbl) {
            $tbl->dropForeign('people_account_id_foreign');
            // $tbl->dropForeign(['account_id']);
            $tbl->dropColumn('account_id');
        });
    }
}
