<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_person', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('people');
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
public function down()
{
    Schema::table('category_person', function (Blueprint $tbl) {
        // Makes up the wrong fk constraint name. I thought it would remove
        // those two FK constraints...
        // $tbl->dropForeign(['category_id', 'person_id']);
        // Works with two separate calls, even though we are using array notation.
        // $tbl->dropForeign(['category_id']);
        // $tbl->dropForeign(['person_id']);
        $tbl->dropForeign('category_person_category_id_foreign');
        $tbl->dropForeign('category_person_person_id_foreign');
    });
    Schema::dropIfExists('category_person');
}
}
