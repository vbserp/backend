<?php

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $yoda = VBSERP\Person::create([
            'name' => 'Mestre Yoda',
            'account_id' => 2,
        ]);

        $luke = VBSERP\Person::create([
            'name' => 'Luke Skywalker',
            'account_id' => 1,
        ]);

    }
}
