<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $demoAccount = VBSERP\Account::find(1);

        $demoAccount->users()->create([
            'name' => 'Fernando Main Demo',
            'email' => 'fernando.basso@vbsmidia.com.br',
            'password' => bcrypt('123456'),
        ]);

        $theOne = VBSERP\Account::find(2);

        $theOne->users()->create([
            'name' => 'Master Yoda',
            'email' => 'yoda@theforce.io',
            'password' => bcrypt('123456'),
        ]);
    }
}
