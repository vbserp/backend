<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Order here is important because some seeds need to fetch
        // previously inserted stuff into the DB so that relationships
        // between models work.
        //
        $this->call(AccountsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(PeopleAddressesTableSeeder::class);
        $this->call(AddressesTableSeeder::class);
    }
}
