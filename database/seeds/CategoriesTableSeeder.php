<?php

use Illuminate\Database\Seeder;
use VBSERP\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat1 = Category::create([
            'name' => 'Category One',
            'slug' => 'category-one',
            'account_id' => 1,
        ]);
        $cat2 = Category::create([
            'name' => 'Category Two',
            'slug' => 'category-two',
            'account_id' => 1,
        ]);
        $sub1 = Category::create([
            'name' => 'Sub Category One',
            'slug' => 'sub-category-one',
            'parent_id' => $cat1->id,
            'account_id' => 1,
        ]);
        $sub2 = Category::create([
            'name' => 'Sub Category Two',
            'slug' => 'sub-category-two',
            'parent_id' => $cat1->id,
            'account_id' => 1,
        ]);
    }
}
