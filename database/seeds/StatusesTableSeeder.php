<?php

use Illuminate\Database\Seeder;

use VBSERP\Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $registeredStatus = Status::create([
            'name' => 'Active',
            'slug' => 'active',
            'account_id' => 1,
        ]);

        $associateStatus = Status::create([
            'name' => 'Inactive',
            'slug' => 'inactive',
            'account_id' => 1,
        ]);
    }
}
