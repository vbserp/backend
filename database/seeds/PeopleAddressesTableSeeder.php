<?php

use Illuminate\Database\Seeder;

class PeopleAddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $yoda = VBSERP\Person::create([
            'name' => 'Mestre Yoda',
        ]);

        $luke = VBSERP\Person::create([
            'name' => 'Luke Skywalker',
        ]);

        $yoda->addresses()->createMany([
            [
                'description' => 'Yoda Force Institute',
                'email' => 'contact-yoda@yodaclinic.io',
            ],
            [
                'description' => 'Mind Space',
                'email' => 'support-mind@mindspace.net',
            ],
        ]);

        $luke->addresses()->createMany([
            [
                'description' => 'Jedi Academy and the user of the force',
                'email' => 'academy-force@theforce.io',
            ],
        ]);
    }
}
