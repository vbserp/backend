<?php

use Illuminate\Database\Seeder;
use VBSERP\Person;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $yoda = Person::where('name', 'LIKE', '%yoda%')->first();

        if (!$yoda) {
            echo "WARNING: user `yoda' not found. Not seeding addresses..." . PHP_EOL;
        }
        else {
            $yoda->addresses()->createMany([
                [
                    'description' => 'Yoda Force Institute',
                    'email' => 'contact@yodaclinic.io',
                ],
                [
                    'description' => 'Mind Space',
                    'email' => 'support@mindspace.net',
                ],
            ]);
        }

        $luke = Person::where('name', 'LIKE', '%luke%')->first();

        if (!$luke) {
            echo "WARNING: user `luke' not found. Not seeding addresses..." . PHP_EOL;
        }
        else {
            $luke->addresses()->createMany([
                [
                    'description' => 'Jedi Academy and the user of the force',
                    'email' => 'academy@theforce.io',
                ],
            ]);
        }
    }
}
