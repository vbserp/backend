<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $demoAccount = VBSERP\Account::create([
            'name' => 'Main Demo',
            'plan' => 'demo',
        ]);

        $ameplan = VBSERP\Account::create([
            'name' => 'The One',
            'plan' => 'basic',
        ]);

    }
}
