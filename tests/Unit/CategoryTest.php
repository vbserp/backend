<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use VBSERP\Category;
use VBSERP\Account;

class CategoryTest extends TestCase
{

    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    /**
     * @test
     */
    public function should_have_a_category()
    {
        $this->assertDatabaseHas('categories', [
            'name' => 'Category One',
            'slug' => 'category-one',
        ]);
    }

    /**
     * @test
     */
    public function should_have_a_sub_category()
    {
        $this->assertDatabaseHas('categories', [
            'name' => 'Sub Category One',
            'slug' => 'sub-category-one',
        ]);
    }

    /**
     * @test
     */
    public function can_insert_category()
    {
        Category::create([
            'name' => 'Foo',
            'slug' => 'foo',
            'account_id' => 1,
        ]);

        $this->assertDatabaseHas('categories', [
            'name' => 'Foo',
            'slug' => 'foo',
            'account_id' => 1,
        ]);
    }

    /**
     * @test
     */
    public function can_update_category()
    {
        $cat = Category::create([
            'name' => 'Foo',
            'slug' => 'foo',
            'account_id' => 1,
        ]);

        $cat->update([
            'name' => 'Foo Edit',
            'slug' => 'foo-edit',
        ]);

        $this->assertDatabaseHas('categories', [
            'name' => 'Foo Edit',
            'slug' => 'foo-edit',
        ]);
    }

    /**
     * @test
     */
    public function can_delete_category()
    {
        $cat = Category::create([
            'name' => 'Foo',
            'slug' => 'foo',
            'account_id' => 1,
        ]);

        $cat->delete();

        $this->assertDatabaseMissing('categories', [
            'name' => 'Foo',
        ]);
    }
}
