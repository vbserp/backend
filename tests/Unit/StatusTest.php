<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    public function testShouldHaveDefaultActiveStatus()
    {
        $this->assertDatabaseHas('statuses', [
            'name' => 'Active',
            'slug' => 'active',
            'account_id' => 1,
        ]);

        $this->assertDatabaseHas('statuses', [
            'name' => 'Inactive',
            'slug' => 'inactive',
            'account_id' => 1,
        ]);
    }

}
