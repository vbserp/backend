<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use VBSERP\User;

class CategoryCrudTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    /**
     * @test
     *
     * Not logged in users should be redirected to login.
     */
    public function not_logged_in_user_cannot_see_categories()
    {
        //$user = factory(User::class)->create();
        $response = $this->get('/categories');
        $response
            ->assertStatus(302)
            ->assertRedirect('/login');
    }

    /**_
     * @test
     *
     * User must be logged in to see categories.
     */
    public function logged_in_user_can_see_categories()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/categories');

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Category One',
            ])
            ->assertJsonFragment([
                'name' => 'Sub Category Two',
            ]);
    }
}

