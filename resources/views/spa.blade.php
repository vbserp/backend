<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>VBSERP - By VBS Mídia</title>
    <link rel="stylesheet prefetch" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <link rel="shortcut icon" href="/dist/favicon.png">
    <meta name="_token" id="csrf-token" content="{{ csrf_token() }}" />
    <script type="text/javascript">
        VBSERP = {
            baseURL: '{{ url('/') }}',
            csrfToken: '{{ csrf_token() }}',
        };
    </script>
  </head>
  <body>
    <div id="vueroot"></div>
    <script type="text/javascript" src="/dist/build.js"></script>
  </body>
</html>

