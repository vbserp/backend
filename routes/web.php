<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {

    //Route::get('/', function () {
    //    return \File::get(public_path() . '/dist/index.html');
    //});

    // Get info from current logged in user and its related parent account.
    Route::get('/user/details', 'UserController@details')->name('user-details');

    Route::get('/', 'HomeController@index')->name('spa');

    Route::get('/categories', 'CategoryController@index')->name('category-index');
    Route::post('/categories', 'CategoryController@create')->name('category-create');
    Route::get('/categories/{category_id}', 'CategoryController@edit')->name('category-edit');
    Route::patch('/categories/{category_id}', 'CategoryController@update')->name('category-update');

    Route::get('/statuses', 'StatusController@index')->name('status-index');
    Route::post('/statuses', 'StatusController@create')->name('status-create');
    Route::get('/statuses/{status_id}', 'StatusController@edit')->name('status-edit');
    Route::patch('/statuses/{status_id}', 'StatusController@update')->name('status-update');

    Route::get('/people', 'PersonController@index')->name('person-index');
    Route::Post('/people', 'PersonController@create')->name('person-create');
    Route::get('/people/{person_id}', 'PersonController@findById')->name('person-find-by-id');
    Route::patch('/people/{person_id}', 'PersonController@update')->name('person-update');
});
