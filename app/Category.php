<?php

namespace VBSERP;

use Illuminate\Database\Eloquent\Model;
use VBSERP\Account;

class Category extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'account_id',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function people()
    {
        return $this->belongsToMany(Person::class);
    }
}
