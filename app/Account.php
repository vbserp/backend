<?php

namespace VBSERP;

use Illuminate\Database\Eloquent\Model;
use VBSERP\User;
use VBSERP\Category;
use VBSERP\Status;
use VBSERP\People;

class Account extends Model
{
    protected $fillable = [
        'name',
        'plan', // demo, free, basic, pro, premium (strings).
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function statuses()
    {
        return $this->hasMany(Status::class);
    }

    public function people()
    {
        return $this->hasMany(Person::class);
    }
}
