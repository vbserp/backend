<?php

namespace VBSERP;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'account_id',
    ];


    public function accounts()
    {
        return $this->belongsTo(Account::class);
    }

    public function people()
    {
        return $this->hasMany(Person::class);
    }
}
