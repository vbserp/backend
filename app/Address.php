<?php

namespace VBSERP;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'description',
        'street',
        'number',
        'room',
        'fax',
        'city',
        'state',
        'cep',
        'landline_phone',
        'cell_phone',
        'email',
        'observation',
        'created_at',
        'updated_at',
    ];


    public function people()
    {
        return $this->belongsToMany(Person::class);
    }
}
