<?php

namespace VBSERP;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = [
        'name',
        'email',
        'landline_phone',
        'cell_phone',
        'union_code',
        'observation',
        'created_at',
        'updated_at',
        'account_id',
        'status_id',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function addresses()
    {
        return $this->belongsToMany(Address::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}

