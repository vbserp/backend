<?php

namespace VBSERP\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use VBSERP\Category;

class CategoryController extends Controller
{
    public function index()
    {
        return Auth::user()->account->categories->all();
    }

    public function create(Request $req)
    {
        $category = new Category();
        $category->name = $req->input('name');
        $category->slug = $category->name;
        $category->account()->associate(Auth::user()->account);
        $category->save();
        return $category;
    }

    public function edit(Request $req, $category_id)
    {
        return Category::find($category_id);
    }

    public function update(Request $req, $category_id)
    {
        $category = Category::find($category_id);
        $category->name = $req->input('name');
        $category->slug = str_slug($category->name);
        $category->save();
        return $category;
    }
}
