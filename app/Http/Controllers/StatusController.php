<?php

namespace VBSERP\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use VBSERP\Status;

class StatusController extends Controller
{
    public function index()
    {
        return Auth::user()->account->statuses->all();
    }

    public function create(Request $req)
    {
        $status = Status::create([
            'name' => $req->input('name'),
            'account_id' => Auth::user()->account->id,
            'slug' => str_slug($req->input('name')),
        ]);

        // This is somehow returning status === 201 automagically (which
        // is what the RFCs say about creating an entity). So, it is
        // correct indeed.
        return $status;
    }

    public function edit(Request $req, $status_id)
    {
        return Status::find($status_id);
    }

    public function update(Request $req, $status_id)
    {
        $status = Status::find($status_id);
        $status->name = $req->input('name');
        $status->slug = str_slug($status->name);
        $status->save();
        return $status;
    }
}

