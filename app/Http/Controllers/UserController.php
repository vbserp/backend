<?php

namespace VBSERP\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use VBSERP\User;

class UserController extends Controller
{
    public function details()
    {
        return User::with('account')->find(Auth::user()->id);
    }
}


