<?php

namespace VBSERP\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use VBSERP\Person;
Use VBSERP\Address;
use VBSERP\Status;
use VBSERP\Category;

class PersonController extends Controller
{
    public function index()
    {
        return Auth::user()->account->people;
    }


    /**
     * Finds a person by their id.
     *
     * @param Request $req
     * @param int $person_id
     * @return json - a person object.
     */
    public function findById(Request $req, $person_id)
    {
        return Person::with('status', 'categories')->find($person_id);
    }

    public function create(Request $req)
    {
        $person = Person::create([
            'name' => $req->input('person')['name'],
            'email' => $req->input('person')['email'],
            'landline_phone' => $req->input('person')['landline_phone'],
            'cell_phone' => $req->input('person')['cell_phone'],
            'union_code' => $req->input('person')['union_code'],
            'status_id' => $req->input('status_id'),
            'account_id' => Auth::user()->account->id,
        ]);

        $person->categories()->sync($req->input('categories_ids'));
        return $person;
    }

    public function update(Request $req, $person_id)
    {
        $person = Person::find($person_id);
        $person->update([
            'name' => $req->input('person')['name'],
            'email' => $req->input('person')['email'],
            'landline_phone' => $req->input('person')['landline_phone'],
            'cell_phone' => $req->input('person')['cell_phone'],
            'union_code' => $req->input('person')['union_code'],
            'status_id' => $req->input('status_id'),
        ]);

        $person->categories()->sync($req->input('categories_ids'));

        return $person;
    }
}
